# Bannière Financement participatif Tour Alternatiba 2024

Bannière temporaire pour le financement participatif du Tour Alternatiba 2024

## Code HTML 

* Dans l'éditeur de thèmes, modifier `header.php` du thème ANV-COP21 officiel : https://alternatiba.eu/wp-admin/theme-editor.php?file=header.php&theme=anv-cop21
* Inclure le code dans la balise `<header>`
* Mettre à jour le fichier

## Code CSS

* Dans l'éditeur de thèmes, modifier `style.css` du thème ANV-COP21 officiel : https://alternatiba.eu/wp-admin/theme-editor.php?file=style.css&theme=anv-cop21
* Inclure le code dans la feuille de style
* Mettre à jour le fichier

## Voir aussi

https://framagit.org/alternatiba/website/popup-financement-participatif-tour-alternatiba-2024

